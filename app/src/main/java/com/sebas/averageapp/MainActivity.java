package com.sebas.averageapp;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText et1;
    private EditText et2;
    private EditText et3;
    private TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Match attributes with forms
        et1 = (EditText)findViewById(R.id.txtMath);
        et2 = (EditText)findViewById(R.id.txtPhy);
        et3 = (EditText)findViewById(R.id.txtChem);
        tv1 = (TextView)findViewById(R.id.txtStatus);
    }

    public void status(View view){
        String mathsStr = et1.getText().toString();
        String phyStr = et2.getText().toString();
        String chemStr = et3.getText().toString();

        // Parse to int the form values
        int mathsInt = Integer.parseInt(mathsStr);
        int phyInt = Integer.parseInt(phyStr);
        int chemInt = Integer.parseInt(chemStr);

        int avr = (mathsInt + phyInt + chemInt)/3;
        if (avr >= 6) {
            tv1.setText("Approved: " + avr);
        } else {
            tv1.setText("Reprobate: " + avr);
        }
    }

}